# USB-RS232 Interface Hardware

CH340G + MAX232 USB-RS232 Interface Hardware. Author: SUF
KiCAD + OpenSCAD design files

## Versions

- V1.0 - First version. Based on ATMEGA328P + CH340G
- V1.0a - Based on ATMEGA328P + CH340G. Minor fixes
- V1.1  - Based on ATMEGA328P + CH340G. Two sided USB connector/ISP connector placement (allow to use either Male or Female GPIB connector)

## Software
The hardware is planed to be compatible with the [AR488](https://github.com/Twilight-Logic/AR488) implementation

## Reference
[Prologix GP-IB USB Controller](https://prologix.biz/downloads/PrologixGpibUsbManual-6.0.pdf)
[IEEE-488.1-2003 Standard](https://standards.ieee.org/standard/488_1-2003.html)
[SCPI Specification](http://www.ivifoundation.org/docs/scpi-99.pdf)

Special thanks to Emanuele Girlando for the inspiration of this work:
[The original article](http://egirland.blogspot.com/2014/03/arduino-uno-as-usb-to-gpib-controller.html)

License:
[This work is licensed under a Creative Commons
Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0) license](https://creativecommons.org/licenses/by-nc-sa/4.0/)