$fn=180;

use <parts/RS232USB_bottom_uUSB.scad>
use <parts/RS232USB_top_uUSB.scad>

translate([0,0,22.55])
{
    color("lightgray")
    {
        import("parts/RS232USB_uUSB.stl");
    }
}



color("lightblue")
{
    RS232USB_bottom_uUSB();
}

translate([0,0,30])
{
    color("LightCoral")
    {
        RS232USB_top_uUSB();
    }
}