$fn=180;

use <parts/RS232USB_bottom_cable.scad>
use <parts/RS232USB_top_cable.scad>

translate([0,0,22.55])
{
    color("lightgray")
    {
        import("parts/RS232USB_cable.stl");
    }
}


color("lightblue")
{
    RS232USB_bottom_cable();
}

translate([0,0,30])
{
    color("LightCoral")
    {
        RS232USB_top_cable();
    }
}