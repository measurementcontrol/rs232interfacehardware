$fn=180;
module RS232USB_bottom_uUSB()
{
    translate([25.3,-5,0])
    {
        cube([2,10,3]);
    }
    for(i=[-1,1])
    {
        difference()
        {
            translate([19.6,13.5*i,4.15]) cube([10,9,8.3],center=true);
            translate([24,12*i,6.15]) cube([10,7,8.3],center=true);    
            translate([19.6,12.5*i,8.3])
                rotate([0,90,0])
                    cylinder(d=4,h=10.002,center=true);
        }
    }    
    difference()
    {
        union()
        {
            hull()
            {
                
                for(i=[-1,1])
                {
                    translate([25.3,9*i,0]) cylinder(d=0.001,h=2);
                    translate([-16.5,7*i,0]) cylinder(d=4,h=2);
                    translate([-21.5,4*i,0]) cylinder(d=4,h=2);
                }
            }
            translate([0,0,2]) cylinder(d=8,h=5.55);
        }
        translate([0,0,-0.001])
        {
            cylinder(d=3.5,h=7.552);
            cylinder(d=6.7,h=3,$fn=6);
        }
    }

    difference()
    {
        hull()
        {
            translate([20,0,7]) cube([0.001,18,10],center=true);
            for(i=[-1,1])
            {     
                translate([-16.5,7 * i,2]) cylinder(d=4,h=10);
                translate([-21.5,4 * i,2]) cylinder(d=4,h=10);
            }
        }
        hull()
        {
            translate([20.001,0,6.999]) cube([0.001,15.4,10.002],center=true);
            for(i=[-1,1])
            {
                translate([-16.5,6.7*i,1.999]) cylinder(d=2,h=10.002);
                translate([-21,4*i,1.999]) cylinder(d=2.3,h=10.002);
            }
        }
        hull()
        {
            translate([20.001,0,11.5]) cube([0.001,17,1.002],center=true);
            for(i=[-1,1])
            {
                translate([-16.5,7.5*i,11]) cylinder(d=2,h=1.002);
                translate([-21.75,4.5*i,11]) cylinder(d=2.3,h=1.002);
            }
        }
        // cutout
        translate([-24.001,-4,9]) cube([3,8,3.001]);
    }
    for(i=[-1,1])
    {
        for(j=[-1,1])
        {
            translate([14*i+2,7.3*j,1.999]) cylinder(d=2,h=5.55);
        }
    }
}
RS232USB_bottom_uUSB();


