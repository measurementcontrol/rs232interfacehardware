$fn=180;
module RS232USB_top_uUSB()
{
    translate([25.3,-5,13.6]) cube([2,10,3]);
    for(i=[-1,1])
    {
        difference()
        {
            translate([19.6,13.5*i,12.45]) cube([10,9,8.3],center=true);
            translate([24,12*i,10.45]) cube([10,7,8.3],center=true);    
            translate([16.8,9.25*i,10.15]) cube([4.402,0.502,3.702],center=true);
            translate([19.6,12.5*i,8.3])
                rotate([0,90,0])
                    cylinder(d=4,h=10.002,center=true);
        }
    }


    difference()
    {
        union()
        {
            hull()
            {
                for(i=[-1,1])
                {            
                    translate([25.3,9*i,14.6]) cylinder(d=0.001,h=2);
                    translate([-16.5,7*i,14.6]) cylinder(d=4,h=2);
                    translate([-21.5,4*i,14.6]) cylinder(d=4,h=2);
                }
            }
            translate([0,0,9.15]) cylinder(d=6,h=5.5);
        }
        translate([0,0,9.149]) cylinder(d=3.5,h=7.502);
        translate([0,0,13.601]) cylinder(d1=0.001,d2=6,h=3);
    }

    difference()
    {
        union()
        {
            hull()
            {
                for(i=[-1,1])
                {     
                    translate([20,9 * i,12]) cylinder(d=0.001,h=2.6);
                    translate([-16.5,7 * i,12]) cylinder(d=4,h=2.6);
                    translate([-21.5,4 * i,12]) cylinder(d=4,h=2.6);
                }
            }
            hull()
            {
                for(i=[-1,1])
                {
                    translate([20,8.25*i,11]) cylinder(d=0.001,h=1.002);
                    translate([-16.5,7.25*i,11]) cylinder(d=2,h=1.002);
                    translate([-21.5,4.25*i,11]) cylinder(d=2.3,h=1.002);
                }
            }    
        }
        hull()
        {
            for(i=[-1,1])
            {
                translate([20.001,7.7*i,10.998]) cylinder(d=0.001,h=3.603);
                translate([-16.5,6.7*i,10.998]) cylinder(d=2,h=3.603);
                translate([-21,4*i,10.998]) cylinder(d=2.3,h=3.603);
            }
        }
        translate([-23,-4,9]) cube([3,8,3.001]);
    }
}

RS232USB_top_uUSB();